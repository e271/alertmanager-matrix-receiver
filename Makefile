# Makefile
#
# deploy
# alertmanager-matrix-receiver.py
#
# a webhook receiver for alertmanager which forwards messages to matrix
#


THIS_FILE := $(lastword $(MAKEFILE_LIST))
DEST := $(HOME)/kube-prometheus-manifests/overlay/alertmanager-matrix-receiver.yaml
TPL_DEPLOY := alertmanager-matrix-receiver-deployment.yaml.tpl
TPL_SECRET := alertmanager-matrix-receiver-secret.yaml.tpl

export NAME := alertmanager-matrix-receiver
export KUBE_NAMESPACE := monitoring


# these will be stored as secrets hence base64 encoded
# if MATRIX_ROOM is not specified the existing secret will not be touched
# MATRIX_URL := ""
# MATRIX_TOKEN := ""
# MATRIX_ROOM := ""





build:
	echo "# $(shell basename $(DEST))" > deployment.yaml

  ifdef $(MATRIX_TOKEN)
	envsubst < $(TPL_SECRET) >> deployment.yaml
	echo "  MATRIX_TOKEN: $(MATRIX_TOKEN)" >> deployment.yaml
  ifdef $(MATRIX_URL)
	echo "  MATRIX_URL: $(MATRIX_URL)" >> deployment.yaml
  endif
  ifdef $(MATRIX_ROOM)
	echo "  MATRIX_ROOM: $(MATRIX_ROOM)" >> deployment.yaml
  endif
  endif
	envsubst < $(TPL_DEPLOY) >> deployment.yaml
	sed -r 's/^/    /' alertmanager-matrix-receiver.py >> deployment.yaml
	cp -v deployment.yaml $(DEST)


status:
	kubectl -n $(KUBE_NAMESPACE) get deployment $(NAME)

debug: build
	cat deployment.yaml

apply:
	$(info only run for debug or if not using kustomization)
	kubectl -n $(KUBE_NAMESPACE) apply -f deployment.yaml


all: build
	kubectl -n $(KUBE_NAMESPACE) apply -f deployment.yaml
	@$(MAKE) -f $(THIS_FILE) status


logs:
	POD_NAME := $(shell kubectl -n $(KUBE_NAMESPACE) get deployment $(NAME) -o custom-columns=NAME:.metadata.name | tail -n1)
	kubectl -n $(KUBE_NAMESPACE) logs $(POD_NAME)


.PHONY: status build all logs debug apply
