---
apiVersion: v1
kind: Secret
metadata:
  name: $NAME
  namespace: $KUBE_NAMESPACE
type: Opaque
data:
  DEBUG: dHJ1ZQ==
