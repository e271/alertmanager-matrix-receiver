---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: $NAME
  namespace: $KUBE_NAMESPACE
spec:
  replicas: 1
  selector:
    matchLabels:
      app: $NAME
  template:
    metadata:
      labels:
        app: $NAME
    spec:
      containers:
      - name: $NAME
        imagePullPolicy: IfNotPresent
        image: "python:3-alpine"
        resources:
          requests:
            memory: 128Mi
            cpu: 150m
        tty: true
        ports:
        - containerPort: 8000
          name: http
        command: ["/bin/sh"]
        args:
          - "-x"
          - "-c"
          - >
            mkdir -p /dev/shm/dotexporter; export HOME=/dev/shm/dotexporter;
            pip install --no-warn-script-location --user --no-cache-dir requests jinja2;
            exec python3 /srv/alertmanager-matrix-receiver.py
        envFrom:
          - secretRef:
              name: $NAME
        volumeMounts:
          - name: $NAME
            mountPath: /srv
        readinessProbe:
          httpGet:
            path: /health
            port: http
          initialDelaySeconds: 10
          periodSeconds: 60
        livenessProbe:
          httpGet:
            path: /health
            port: http
          initialDelaySeconds: 20
          periodSeconds: 60
      securityContext:
        runAsUser: 1000
        fsGroup: 1000
      volumes:
        - name: $NAME
          configMap:
            name: $NAME
---
apiVersion: v1
kind: Service
metadata:
  name: $NAME
  namespace: $KUBE_NAMESPACE
  labels:
    app: $NAME
spec:
  ports:
  - port: 8000
    name: http
  selector:
    app: $NAME
  sessionAffinity: None
  type: ClusterIP
  # clusterIP: None
---
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  name: $NAME
  namespace: $KUBE_NAMESPACE
spec:
  endpoints:
  - interval: 1m
    port: http
  selector:
    matchLabels:
      app: $NAME
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: $NAME
  namespace: $KUBE_NAMESPACE
data:
  alertmanager-matrix-receiver.py: |
