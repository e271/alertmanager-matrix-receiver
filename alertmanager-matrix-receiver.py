#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# prometheus alertmanager webhook receiver endpoint which forwards posts to a 
# matrix server
#
# https://prometheus.io/docs/alerting/configuration/#webhook_config
#
# e.g.
# webhook_configs:
#   - url: http://alertmanager-matrix-receiver:8000/room/%21fullroomaddress
#
# TODO
# - metrics


from http.server import ThreadingHTTPServer, BaseHTTPRequestHandler

import requests
import os

# from urllib.parse import quote
import json

from jinja2 import Template




LISTEN       = os.environ.get("LISTEN", "0.0.0.0")
PORT         = int(os.environ.get("PORT", "8000"))
DEBUG        = bool(os.environ.get("DEBUG", False))
MATRIX_ROOM  = os.environ.get("MATRIX_ROOM", "")

try:
  MATRIX_TOKEN = os.environ["MATRIX_TOKEN"]
  MATRIX_URL   = os.environ["MATRIX_URL"]
except:
  print("MATRIX_TOKEN and MATRIX_URL environment variables required")
  exit(1)


# android client doesn't know all of the colors :(
# e.g. deeppink = #FF1493, darkgreen = #006400, greenyellow = #ADFF2F, 
# limegreen = #32CD32

MSG = Template('''
{% for alert in alerts %}
<strong>[<font
{% if alert.labels.severity == "dummy" %}
  color="{% if alert.status == "firing" %}#FF1493{% else %}#006400{% endif %}"
{% elif alert.labels.severity == "notification" %}
  color="{% if alert.status == "firing" %}yellow{% else %}#ADFF2F{% endif %}"
{% elif alert.labels.severity == "warning" %}
  color="{% if alert.status == "firing" %}orange{% else %}#32CD32{% endif %}"
{% else %}{# severity undefined resp critical #}
  color="{% if alert.status == "firing" %}red{% else %}green{% endif %}"
{% endif %}
>{{ alert.status|upper }}
{% if receiver == "alertmanager-matrix-receiver" %}
: {{ alert.labels.alertname }}</font>]</strong>
<a href="{{ alert.annotations.runbook_url }}" style="text-decoration:none"
>{{ alert.annotations.message }}</a><br />
{% else %}
</font>] {{ alert.annotations.summary }}</strong>

{% if not alert.annotations.link and alert.annotations.description %}
</p title="started at {{ alert.startsAt }}">{{ alert.annotations.description }}</p>
{% else %}
<p><a href="{{ alert.annotations.link }}">{{ alert.annotations.description }}</a>
{% if alert.annotations.alt_link_text and alert.annotations.alt_link_url %}
<em><a href="{{ alert.annotations.alt_link_url }}">{{ alert.annotations.alt_link_text }}</a></em>
{% endif %}
{% if alert.annotations.alt_link_text2 and alert.annotations.alt_link_url2 %}
<em><a href="{{ alert.annotations.alt_link_url2 }}">{{ alert.annotations.alt_link_text2 }}</a></em>
{% endif %}
</p>
{% endif %}
{% endif %}
{% endfor %}
''')


class HTTPRequestHandler(BaseHTTPRequestHandler):

  request_count_get  = 0
  request_count_post = 0

  def do_GET(self):
    '''
    used for health and liveness probes
    '''
    HTTPRequestHandler.request_count_get = HTTPRequestHandler.request_count_get + 1

    if self.path == '/health':
      self.send_response(200)
      text = 'OK'
    elif self.path == '/metrics':
      self.send_response(200)
      metric = "http_request_handler_count"
      help = "# HELP %s request count" % metric
      type = "# TYPE %s counter" % metric
      text = '%s\n%s\n%s{method="get"} %s\n%s{method="post"} %s\n' % (
          help, type, metric, HTTPRequestHandler.request_count_get,
                      metric, HTTPRequestHandler.request_count_post
      )
    else:
      self.send_response(404)
      text = 'NOT FOUND'

    self.send_header("Content-type", "text/plain")
    self.end_headers()
    self.wfile.write(str.encode(text))

    return


  def do_POST(self):

    HTTPRequestHandler.request_count_post = HTTPRequestHandler.request_count_post + 1

    header = { 
      'Content-Type': 'application/json',
      'User-Agent': 'alertmanager-matrix-receiver',
      'Authorization' : 'Bearer %s' % MATRIX_TOKEN
    }


    try:

      content_length = int(self.headers['Content-Length'])
      if content_length > 200000:
        raise ValueError('too much data received')
      body = self.rfile.read(content_length)
      alerts = json.loads(body)

      if DEBUG: print("dump: %r" % alerts)

      if self.path.startswith('/room/'):
        url = "%s/_matrix/client/r0/rooms/%s/send/m.room.message" \
          % (MATRIX_URL, self.path.replace('/room/', ''))
      elif MATRIX_ROOM:
        url = "%s/_matrix/client/r0/rooms/%s/send/m.room.message" \
          % (MATRIX_URL, MATRIX_ROOM)
      else:
        raise ValueError('no room specified')


      if alerts["alerts"]:
        # do your own templating here

        msg = {
          'body': '-',
          'msgtype': 'm.text',
          'format': 'org.matrix.custom.html',
          'formatted_body': MSG.render(alerts)
        }

        r = requests.post(url, json=msg, headers=header)
        try:
          print("  status code: %s - id %s" % (r.status_code, r.json["event_id"]))
        except:
          print("  status code: %s" % r.status_code)


        self.send_response(r.status_code)
        self.end_headers()
      else:
        print("> no alerts send for %s" % alerts["receiver"])
        self.send_response(200)
        self.end_headers()
    except:
      print("| %s failed" % self.requestline)
      self.send_response(400)
      self.end_headers()
      if DEBUG: raise

    self.wfile.write(str.encode(''))
    return




if __name__ == '__main__':
  # httpd = HTTPServer((LISTEN, PORT), HTTPRequestHandler)
  httpd = ThreadingHTTPServer((LISTEN, PORT), HTTPRequestHandler)
  print("Serving POST requests on %s:%s" % (LISTEN, PORT))
  httpd.serve_forever()


