# alertmanager-matrix-receiver

a simple webhook receiver for prometheus' alertmanager which forwards the 
messages to a matrix server api.

The receiver is a simple python script which will be wrapped into a configmap 
for running in kubernetes. a python container deployment will run it 
eventually exposing its metrics endpoint to a prometheus operator.



